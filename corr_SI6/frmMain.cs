﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils;
namespace corr_SI6
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
           
            InitializeComponent();
            tbAffiche.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;


        }

    
        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void afficherListeVisiteurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cdao odao = new Cdao();
            string query = "select id, nom, prenom from visiteur";
            MySqlDataReader ord = odao.getReader(query);

            while(ord.Read())
            {
                tbAffiche.Text = tbAffiche.Text + ord["id"] +"  "+ ord["prenom"] + "  "+ ord["nom"] + "\r\n";
            }
            ord.Close();
        }

        private void tbAffiche_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
